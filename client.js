const net = require("net")
const path = require("path")
const fs = require("fs")

const gen_id = () => Math.random().toString(36).substring(1)
const connect = (id, msg) => new Promise(res => {
  const sk = net.createConnection(8266, `192.168.${id}.1`)
  sk.on("connect", () => {
    sk.write(JSON.stringify(msg))
    sk.destroy()
    res()
  })
})


const sendFile = async (id, file) => {
  const content = fs.readFileSync(path.join(__dirname, "chip", file)).toString()
  const msg = {
    path: file,
    content,
    command: 'upload',
    id: gen_id(),
  }
  await connect(id, msg)
}

const restart = id => {
  const msg = {
    command: 'restart',
    id: gen_id()
  }
  connect(id, msg)
}

const ping = id => {
  connect(id, { id: gen_id(), command: 'ping' })
}
sendFile(8, "lib/cmds/restart.lua")
// restart(8)
// ping(8)