const net = require("net")

const server = net.createServer(sk => {
  console.log('server connected');
  sk.on("data", data => console.log(JSON.parse(data.toString())))
  sk.on('end', () => console.log('server disconnected'));
  sk.pipe(sk);
});

server.listen(8266);
