sender = require("lib/sender")

return function(str, cb)
  local msg = {
    command = 'receive_debug_msg',
    content = str
  }

  msg.id = crypto.hash("sha1", sjson.encode(msg))
  sender.add_msg(msg)
end