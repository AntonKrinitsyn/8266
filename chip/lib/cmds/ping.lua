sender = require("lib/net/sender")
utils = require("lib/utils")

return function(msg, cb)
  sender.add_msg({ id = utils.gen_id(), pong = true })
  cb(true)
end