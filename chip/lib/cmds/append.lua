-- cmd to append content to file
return function(msg, cb)
  if file.open(msg.path, 'a+') then
    out.debug('appending ' .. msg.path .. ' size: ' .. #msg.content .. ' symbols')
    file.write(msg.content)
    file.close()
    out.debug('saved')
    return cb(true)
  else
    out.debug('error while opening config file')
    return cb(false, 'can not write to file')
  end
end
