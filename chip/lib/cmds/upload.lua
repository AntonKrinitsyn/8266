-- module to handle file upload
-- command with general purpose is
-- to update FS with new content
-- out = require("lib/out")

-- validates that msg is of correct type
-- if not then throws error
local function validate(msg)
  if type(msg) ~= 'table' then
    error('msg type is ' .. type(msg) .. '. Should be table')
  end

  if not msg.path then
    error('msg should contain path property')
  end

  if not msg.content then
    error('msg should contain content property')
  end
end

-- write msg content to the file
return function(msg, pcb)
  -- out.debug("upload command")
  local ok, err = pcall(validate, msg)

  if not ok then
    -- out.debug('error during validation', err)
    return pcb(false, err)
  end
  -- out.debug("valid msg")

  if file.open(msg.path, 'w+') then
    -- out.debug('uploading ' .. msg.path .. ' size: ' .. #msg.content .. ' symbols')
    file.write(msg.content)
    file.close()
    -- out.debug('saved')
    return pcb(true)
  else
    -- out.debug('error while opening config file')
    return pcb(false, 'can not write to file')
  end
end
