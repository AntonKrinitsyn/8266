-- module to handle file remove
-- command with general purpose
out = require("lib/out")

-- validates that msg is of correct type
-- if not then throws error
local function validate(msg)
  if type(msg) ~= 'table' then
    error('msg type is ' .. type(msg) .. '. Should be table')
  end

  if not msg.path then
    error('msg should contain path property')
  end
end

-- write msg content to the file
return function(msg, pcb)
  out.debug("remove command")
  local ok, err = pcall(validate, msg)

  if not ok then
    out.debug('error during validation', err)
    return pcb(false, err)
  end
  out.debug("valid msg")
  file.remvoe(msg.path)
  out.debug("file was removed")
  pcb(true)
end
