-- module to execute restart command
-- simply restart microcontroller
tmr = require('tmr')
node = require('node')

out = require('lib/out')
wrap = require('lib/wrap')

restart_timeout = 10001

-- restart device
return function(msg, pcb)
  out.debug('before device restart. Restart after 10 seconds')
  tmr.create():alarm(restart_timeout, tmr.ALARM_SINGLE, function() node.restart() end)
  pcb(true)
end