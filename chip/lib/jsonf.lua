-- module provide saving/loading
-- dict data to/from file
-- if you need to store persistant configuration
-- it stores dict as json and decodes to dict
local export = {}

-- loads file from fs. Returns error if file
-- not exists or error occupied during file read
function export.load(file_name)
  if file.open(file_name, 'r') then
    local content = sjson.decode(file.read())
    file.close()
    return content
  else
    error("can't open the file")
  end
end

-- save dict to file in json format. overwrite if
-- file already exists
function export.save(file_name, obj)
  if file.open(file_name, 'w+') then
    file.write(sjson.encode(obj))
    file.close()
  else
    error("can't save the file")
  end
end

return export
