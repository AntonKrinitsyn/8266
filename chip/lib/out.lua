-- module that is capable of
-- basic logging funcionality
-- it can keep track of several
-- listener destination,
-- display json, dict
-- attach loglevel/timestamp to log
env = require("lib/env")

local export = {}
local listeners = {}
local id = env.get("id")


export.DEBUG = 0
export.LOG = 1

local function get_lvl_name(lvl)
  if lvl == export.DEBUG then return 'DEBUG' end
  if lvl == export.LOG then return 'LOG' end
  return 'UNKNOWN'
end

-- take all subscribed function and
-- send msg there. If error returnd from
-- subscriber, then remove it from
-- the subscription list
local function send_to_listeners(lvl, ...)
  for id, listener in pairs(listeners) do
    local fn, min_listener_lvl = unpack(listener)
    if string.find(type(fn), 'function') and min_listener_lvl <= lvl then
      local log_lvl_name = get_lvl_name(lvl)
      local min_listener_lvl_name = get_lvl_name(min_listener_lvl)
      local info =
        '#listner=' .. id .. ' ' ..
        '#heap=' .. node.heap() .. ' ' ..
        '#log_lvl=' .. log_lvl_name .. ' ' ..
        '#time=' .. tmr.time() .. ' ' ..
        '#id=' .. id
      local ok = pcall(fn, info, ...)
      if not ok then export.unsub(id) end
    end
  end
end

-- logs msg to all listeners. log is highest log lvl
function export.log(...) send_to_listeners(export.LOG, ...) end

-- debug is lowest log lvl
function export.debug(...) send_to_listeners(export.DEBUG, ...) end

function export.throw(msg) export.debug(msg) error(msg) end

-- dictionary specific log
function export.dict(dict)
  for k,v in pairs(dict) do export.log(k, v) end
end

-- dictionary to encoded json log
function export.json(val) export.log(sjson.encode(val)) end

-- add listener
function export.sub(key, fn, lvl)
  if not lvl then lvl = export.LOG end
  listeners[key] = { fn, lvl }
end

-- remove listener
function export.unsub(key)
  local new_listeners = {}
  for k, v in pairs(listeners) do
    if k ~= key then new_listeners[k] = v end
  end
  listeners = new_listeners
end

-- remove all listeners
function export.clean() listeners = {} end

function export.lvl(id, lvl)
  if not listeners[id] then return end
  listeners[id][2] = lvl
end

-- subscribe default listener(print)
export.sub('def', print, export.DEBUG)

return export