-- module wraps jsonf module
-- to create high level commanxs to
-- persistantly store key/value
-- pairs in flesh memory
jsonf = require('lib/jsonf')

local export = {}
local path = 'config.json'

-- set key/value pair
function export.set(key, val)
  local env = jsonf.load(path)
  env[key] = val
  jsonf.save(path, env)
end

-- get value by key
function export.get(key)
  local env = jsonf.load(path)
  val = env[key]
  return val
end


-- insure that file exists
if not file.exists(path) then jsonf.save(path, {}) end

return export