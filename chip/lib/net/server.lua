out = require('lib/out')
env = require("lib/env")

local export = {}
server = nil

-- handle data receive
local function handle_receive(socket, data)
  local port, ip = socket:getpeer()
  socket:close()
  out.debug("receive", port, ip)
  local ok, result = pcall(sjson.decode, data)
  out.debug(result)
  out.json(result)
  
  if not ok then
    out.debug(ok, result)
    return
  end

  pcall(export.handle_msg, result)
end

--listen for new socket connections
local function listener(socket) socket:on('receive', handle_receive) end

-- start server to listen for msgs
function export.up() 
  server = net.createServer()
  server:listen(env.get("port"), listener)
end

local function validate_msg(msg)
  if type(msg) ~= 'table' then out.throw('msg type is ' .. type(msg) .. '. Should be table') end
  if not msg.id then out.throw('msg does not have id') end
  if msg.to and msg.to ~= env.get("id") then out.throw('msg is not for this chip') end
  if not msg.command then out.debug('msg does not have command') end
end

function export.handle_msg(msg)
  out.debug("start handling msg")
  local cb = wrap(function(ok, data)
    if not ok then
      out.debug(ok, data)
      out.debug('was not able to finish msg successfuly')
      return
    end
    out.debug("msg finished processing")
  end)
  out.debug('validating msg')
  local ok, result = pcall(validate_msg, msg)

  if not ok then
    out.debug('msg is not valid: ' .. result)
    return cb(false, result)
  end

  local cmd_path = 'lib/cmds/' .. msg.command
  out.debug('script path is ' .. cmd_path)

  out.debug('requiring script')
  local ok, result = pcall(require, cmd_path)

  if not ok then
    out.debug('script is not available: ' .. result)
    return cb(false, result)
  end

  out.debug('calling script')
  local ok, err = pcall(result, msg, cb)

  if not ok then
    out.debug('error while executing script')
    return cb(false, err)
  end
end

return export