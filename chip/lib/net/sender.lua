aps = require("lib/net/aps")
wrap = require("lib/wrap")
env = require("lib/env")
out = require("lib/out")
jsonf = require("lib/jsonf")

local export = {}
local store_capacity = 1
local batch_default_size = 1
local processing_timer_delay = 5000

function export.get_batch(batch_size)
  local batch = {}

  if not batch_size then batch_size = batch_default_size end
  out.debug("before listing files")
  local msg_files_obj = file.list("msgs/*")
  local msg_file_paths = {}
  for k, _ in pairs(msg_files_obj) do table.insert(msg_file_paths, k) end
  print(#msg_file_paths)
  if #msg_file_paths == 0 then return batch end

  out.debug("here1")

  if #msg_file_paths < batch_size then batch_paths = #msg_file_paths end
  out.debug("here2. before loop")

  for i=1,batch_size do
    out.debug(msg_file_paths[i])
    ok, msg = pcall(jsonf.load, msg_file_paths[i])
    print(msg)
    if ok then
      table.insert(batch, msg)
      file.remove(msg_file_paths[i])
    end

  end

  return batch
end

function export.remove_msg(msg)
  export.validate(msg)
  return export.remove_msg_by_id(msg.id)
end

function export.remove_msg_by_id(id)
  for idx, i in ipairs(store) do
    export.validate(i)
    if i.id == id then table.remove(store, idx) end
  end
end

function export.remove_batch_from_store(batch)
  for idx, i in ipairs(batch) do export.remove_msg(i) end
end

function export.is_msg_in_store(msg)
  export.validate(msg)
  for _, i in ipairs(store) do
    if msg.id == i.id then return true end
  end
  return false
end

function export.validate(msg)
  if type(msg) ~= 'table' then error("invalid msg") end
  if type(msg.id) == 'nil' then error("msg should contain id") end
end

function export.add_msg(msg)
  local ok, data = export.validate(msg)

  local msg_path = "msgs/" .. msg.id .. ".json"
  local ok, result = pcall(jsonf.save, msg_path, msg)
  out.debug("add msg", ok, result)
end

function export.send(batch, cb)
  local socket = net.createConnection(net.TCP, 0)
  local _, _, gateway = wifi.sta.getip()
  if not gateway then
    cb(false, "gateway is nil")
  end
  local on_disconnected = function() cb(false, "connection is closed") end
  local on_connected = function()
    if #batch == 1 then socket:send(sjson.encode(batch[1])) end
    socket:close()
    cb(true)
  end
  socket:on('connection', on_connected)
  socket:on('disconnection', on_disconnected)
  socket:connect(env.get("port"), gateway)
end

function export.process_new_msgs(cb)
  out.debug(type(wrap))
  out.debug("sender tick")
  local batch = export.get_batch()
  out.debug("after batch")
  out.debug("after removing batch")
  if #batch == 0 then return cb(true) end
  out.debug("after zero  batch")
  local each_do = function(ssid, cb) print(ssid) export.send(batch, wrap(cb)) end
  out.debug("before connect to all")
  aps.connect_to_all_open(each_do, cb)
end

function export.start_processing()
  local timer = tmr.create()
  local update_timer = function() timer:start() end
  local on_tick = function() export.process_new_msgs(wrap(update_timer)) end
  timer:alarm(processing_timer_delay, tmr.ALARM_SEMI, on_tick)
end

return export
