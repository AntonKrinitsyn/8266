out = require("lib/out")

local export = {}

function export.list_open_aps(cb)
  local getap_cb = function(data)
    local list = {}
    local self_ap_config = wifi.ap.getconfig(true)
    local self_ssid = self_ap_config.ssid
    for bssid, info in pairs(data) do
      local ch, auth, rssi, ssid = string.match(
        string.reverse(info),
        '^(.-),(.-),(.-),(.*)$'
      )
      local ap_obj = {
        ssid = string.reverse(ssid),
        rssi = tonumber(string.reverse(rssi)),
        auth_mode = tonumber(string.reverse(auth))
      }
      if ap_obj.auth_mode == wifi.OPEN and ap_obj.ssid ~= self_ssid then
        if not string.find(ap_obj.ssid, 'byfly') then
          table.insert(list, ap_obj)
        end
      end
    end
    cb(true, list)
  end
  wifi.sta.getap({ show_hidden = 1 }, 1, getap_cb)
end

function export.connect_to_ssids(ssids, on_connect, cb)
  if #ssids <= 0 then return cb(true) end
  local random_ssid_index = math.random(#ssids)
  out.json(ssids)
  local ssid = ssids[random_ssid_index]
  out.debug(ssid)
  local tries = 10
  local do_next_ssid = function()
    local next_ssids = {}
    for idx, i in ipairs(ssids) do
      if idx ~= random_ssid_index then table.insert(next_ssids, i) end
    end
    export.connect_to_ssids(next_ssids, on_connect, cb)
  end
  local status = wifi.sta.config({ ssid = ssid, save = save })
  if not status then return do_next_ssid() end
  local try_connect = function(t)
    out.debug("trying to connect")
    if tries <= 0 then return do_next_ssid() end
    local restart_timer = function()
      tries = tries - 1
      t:start()
    end
    local ip, mask, gateway = wifi.sta.getip()
    if not gateway then return restart_timer() end
    on_connect(ssid, do_next_ssid)
  end
  tmr.create():alarm(1000, tmr.ALARM_SEMI,  try_connect)
end

function export.connect_to_all_open(...)
  outer_args = {...}
  local handle_aps_list = function(ok, aps)
    local ssids = {}
    if ok then
      for _, ap in ipairs(aps) do table.insert(ssids, ap.ssid) end
    end
    export.connect_to_ssids(ssids, unpack(outer_args))
  end
  export.list_open_aps(handle_aps_list)
end

return export