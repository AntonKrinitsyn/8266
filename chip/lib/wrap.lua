-- module provides function to decorate
-- callback function so that its called
-- only once and timeout is provided
local default_timeout = 60000

return function(callback, timeout)
  local done = false
  if not timeout then timeout = default_timeout end
  if not callback then callback = function() end end
  local timeout_tmr = tmr.create()
  local on_timeout_expired = function()
    if done then return end
    done = true
    callback(false, 'timeout expired')
  end
  timeout_tmr:alarm(timeout, tmr.ALARM_SINGLE, on_timeout_expired)
  local wrapper = function(...)
    if done then return false, 'cb is done' end
    done = true
    timeout_tmr:unregister()
    callback(...)
    return true
  end
  return wrapper
end
