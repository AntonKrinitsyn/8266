out = require("lib/out")
env = require("lib/env")

sender = require("lib/net/sender")
server = require("lib/net/server")

wifi.setmode(wifi.STATIONAP)

wifi.ap.config({
  ssid = env.get("name"),
  hidden = false,
  auth = wifi.OPEN,
  save = false
})

wifi.ap.setip({
  ip = string.format("192.168.%d.1", env.get("id")),
  netmask = "255.255.255.0",
  gateway = string.format("192.168.%d.1", env.get("id"))
})

sender.start_processing()
server.up()