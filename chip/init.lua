local timer = tmr.create()

local function after_delay(t)
  dofile("main.lua")
end

timer:register(
  2500,
  tmr.ALARM_SINGLE,
  after_delay
)

timer:start()

